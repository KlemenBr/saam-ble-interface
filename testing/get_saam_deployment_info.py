import requests as rs
import csv
from io import StringIO

from functools import lru_cache

import time

@lru_cache(maxsize=None)
def get_sheet_url(path_to_config_file):
    with open(path_to_config_file, "r") as f:
        return f.readline().strip()    

@lru_cache(maxsize=None)
def get_csv(url_file="url.txt"):  # cache consecutive calls
    # return reader containing latest version of BG_SAAM sheet
    res=rs.get(url=get_sheet_url(url_file))
    inp_file = StringIO(res.text)
    return csv.reader(inp_file, delimiter=','), inp_file

def get_macs(column):
    # will return dict, where location is key, and mac addr is value
    result = {}
    csv, _ = get_csv()
    for row in csv:
        # ID's must be one of ATxx, BGxx or SIxx and between, where xx is usually number between 0-99
        location = row[0].strip()
        adv_name = row[column-1].strip()
        mac = row[column].strip()
        if (location.startswith(("AT", "BG", "SI", "XX"))) and (len(location) == 4):
            if len(mac) == 17:
                result[location] = mac, adv_name
    return result

def get_bed():
    return get_macs(6)

def get_bracelet():
    return get_macs(8)

def get_belt():
    return get_macs(10)

def get_linux_id(column):    
    result = {}    
    rows, inp_file =  get_csv() # cached, so reset inp_file on each new call
    inp_file.seek(0)    
    for row in rows:
        # ID's must be one of ATxx, BGxx or SIxx and between, where xx is usually number between 0-99
        location = row[0].strip()
        id = row[column].strip()        
        if (location.startswith(("AT", "BG", "SI", "XX"))) and (len(location) == 4):
            if len(id) != 32: # check that ID is correct lenght
                continue     
            result[id] = location
    return result

def get_egw():
    return get_linux_id(1)


def get_pmc():
    return get_linux_id(2)
    
if __name__ == "__main__":

    for i in get_bed().items():
        print(i)

    for i in get_belt().items():
        print(i)

    for i in get_bracelet().items():
        print(i)

    for i in get_egw().items():
        print(i)

    for i in get_pmc().items():
        print(i)

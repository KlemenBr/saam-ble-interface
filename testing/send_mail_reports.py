#!/usr/bin/env python3

from get_active_saam_egw import (
    get_active_nodes_from_videk,
    get_active_egw,
    get_active_pmc,
)
from get_saam_deployment_info import get_egw, get_pmc

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from datetime import datetime
from time import sleep as sys_sleep
from fabric import Connection
import subprocess
import os
from paramiko.rsakey import RSAKey


SEND_VIDEK_MAIL = False
SEND_MICROHUB_MAIL = False
SEND_UWB_MAIL = False

CHECK_MICROHUB_STATUS = True
UPDATE_MICROHUB_SERVICE = False

CHECK_UWB_STATUS = True

REPORT_NODES_FILENAME = "SAAM_nodes.html"
REPORT_MICROHUB_FILENAME = "SAAM_microhub.html"
REPORT_UWB_FILENAME = "SAAM_UWB.html"

if __name__ == "__main__":

    # get currently online SAAM eGWs nodes (will return as [[ip, id, complete node json]])
    videk_acc = get_active_nodes_from_videk("auth_key.txt")
    online_egw = get_active_egw(videk_acc)[0]
    online_pmc = get_active_pmc(videk_acc)[0]
    print(
        "Currently online SAAM eGWs:",
        len(online_egw),
        "\nCurrently online SAAM PMCs:",
        len(online_pmc),
    )

    # get latest BG_SAAM sheet from internal gdocs
    deployed_egw = get_egw()
    deployed_pmc = get_pmc()
    print(
        "Currently deployed eGWs:",
        len(deployed_egw),
        "\nCurrently deployed PMCs:",
        len(deployed_pmc),
    )

    # combine gdocs and videk results to a temp dict (machine_id, (location, ip))
    def combine_array(m_id, o_id):
        # m_id = (machine_id, location) o_id = (machine_id, ip)

        result = {}
        for e_id in m_id:
            result[e_id] = (
                m_id[e_id],
                o_id[e_id] if (e_id in o_id.keys()) else "OFFLINE",
            )
        # any online device that is not noted in the sheet is also added
        for e_id in set(o_id.keys()) - set(m_id.keys()):
            result[e_id] = "UNKN", o_id[e_id]

        return result

    combined_egw = combine_array(deployed_egw, online_egw)
    combined_pmc = combine_array(deployed_pmc, online_pmc)

    # combine EGW and PMC arrays into a (location, [egws], [pmcs]), seems like there should be a nicer way of doing this...
    combined_all = {}
    for o in combined_egw.values():
        location = o[0]
        temp = combined_all[location] if (location in combined_all.keys()) else {}
        t_egw = temp["egw"] if ("egw" in temp.keys()) else []
        t_egw.append(o[1])
        temp["egw"] = t_egw
        combined_all[location] = temp

    for o in combined_pmc.values():
        location = o[0]
        temp = combined_all[location] if (location in combined_all.keys()) else {}
        t_pmc = temp["pmc"] if ("pmc" in temp.keys()) else []
        t_pmc.append(o[1])
        temp["pmc"] = t_pmc
        combined_all[location] = temp

    # check basic microhub status
    meta = {}
    if CHECK_MICROHUB_STATUS:

        def run_sudo_in_git_dir(conn, cmd):
            return conn.sudo(
                "bash -c 'cd /opt/saam-implementation && " + cmd + "'",
                warn=True,
                hide="out",
            ).stdout.strip()

        def run_command(conn, cmd):
            return conn.run(cmd, warn=True, hide="out").stdout.strip()

        def get_local_git_hash():
            return (
                subprocess.check_output(["git", "rev-parse", "HEAD"])
                .decode("utf-8")
                .strip()
            )

        this_git = get_local_git_hash()

        for loc in combined_all:
            if "egw" not in combined_all[loc]:
                continue
            for ip in combined_all[loc]["egw"]:
                if "OFFLINE" == ip:
                    continue
                try:
                    print(loc, " ", ip)
                    with Connection(
                        host=ip,
                        user="logatec",
                        connect_kwargs={"pkey": RSAKey.from_private_key_file("ssh_key")},
                        connect_timeout=20,
                    ) as conn:
                        info = {
                            "ip": ip,
                            "err": [],
                            "warn": [],
                            "loc": run_command(conn, "cat /etc/lgtc/loc-id"),
                            "git": run_sudo_in_git_dir(conn, "git rev-parse HEAD"),
                            "log": {},
                            "status": run_sudo_in_git_dir(
                                conn, "systemctl status microhub"
                            ),
                        }

                        # mark error
                        if info["loc"] != loc:
                            info["err"].append("LOCATION")

                        # check that git version is the same as on running machine, if not git pull
                        if info["git"] != this_git:
                            info["warn"].append("GIT old " + info["git"][:8])
                            if UPDATE_MICROHUB_SERVICE:
                                # stop service
                                print(
                                    run_sudo_in_git_dir(
                                        conn,
                                        "systemctl stop microhub; rm -f /lib/systemd/system/microhub.service",
                                    )
                                )
                                # handle dependencies
                                print(
                                    run_sudo_in_git_dir(
                                        conn, "pip3 install bluepy paho-mqtt func_timeout"
                                    )
                                )
                                print(run_sudo_in_git_dir(conn, "git pull"))
                                # update systemd service
                                print(
                                    run_sudo_in_git_dir(
                                        conn,
                                        "cp /opt/saam-implementation/microhub.service /etc/systemd/system",
                                    )
                                )
                                print(
                                    run_sudo_in_git_dir(
                                        conn,
                                        'systemctl daemon-reload && systemctl enable microhub && systemctl restart microhub.service && echo "done restarting uhub"',
                                    )
                                )
                                info["git"] = run_sudo_in_git_dir(
                                    conn, "git rev-parse HEAD"
                                )
                                if info["git"] != this_git:
                                    info["err"].append("GIT")
                                sys_sleep(2)
                                print(
                                    run_sudo_in_git_dir(conn, "systemctl status microhub")
                                )

                        # add logs to report
                        with conn.cd("/var/log/SAAM"):
                            info["logs"] = run_command(conn, "ls -1 *.txt")
                            for log in info["logs"].splitlines():
                                name = log.strip()
                                info["log"][name] = run_command(conn, "tail '" + name + "'")

                        # save location to meta dict
                        meta[loc] = info
                except:
                    print("connection timeout")
                    continue
            # break
            
        # send mail as html table with IPs of each location
        if SEND_MICROHUB_MAIL or REPORT_MICROHUB_FILENAME:
            # get smtp auth from file and send the table
            with open("auth_key.txt", "r") as auth_file:

                # we manually build html... not ideal for sure...
                def html_table():
                    # row, column, values
                    table = [
                        [
                            [""],
                            ["loc_id<br><br>IP"],
                            ["err"],
                            ["warn"],
                            ["git"],
                            ["log"],
                            ["status"],
                        ]
                    ]
                    for k in sorted(meta.keys()):
                        logs = ""
                        for l in meta[k]["log"]:
                            l_text = meta[k]["log"][l].replace("\n", "<br>")
                            if len(logs):
                                logs = logs + "<br><br>\n"
                            logs = logs + l + ": <br>" + l_text

                        table.append(
                            [
                                [k],
                                [
                                    str(meta[k]["loc"])
                                    + "<br><br>"
                                    + str(meta[k]["ip"][-4:])
                                ],
                                meta[k]["err"],
                                meta[k]["warn"],
                                [meta[k]["git"][:8]],
                                [logs],
                                [meta[k]["status"].replace("\n", "<br>")],
                            ]
                        )

                    txt = '<table border="01">\n'
                    for row in table:
                        txt = txt + ' <tr align="left">\n  '
                        for column in row:
                            txt = txt + "<th>" + ", ".join(column) + "</th>"
                        txt = txt + " </tr>\n"
                    return txt + "</table>"

                if SEND_MICROHUB_MAIL:
                    _ = auth_file.readline()
                    _ = auth_file.readline()

                    sender_address = auth_file.readline()
                    sender_pass = auth_file.readline()
                    _ = auth_file.readline()
                    receiver_address = [
                        x.strip() for x in auth_file.readline().split(", ")
                    ]

                    mail_content = str(datetime.now()) + "<br>\n" + html_table()

                    message = MIMEMultipart()
                    message["From"] = sender_address
                    message["To"] = ", ".join(receiver_address)
                    message["Subject"] = "SAAM microhub logs: " + str(datetime.now())
                    message.attach(MIMEText(mail_content, "html"))
                    session = smtplib.SMTP("smtp.gmail.com", 587)  # use gmail with port
                    session.starttls()  # enable security
                    session.login(sender_address, sender_pass)
                    text = message.as_string()
                    session.sendmail(sender_address, receiver_address, text)
                    session.quit()
                    print("Mail Microhub Sent")

                if REPORT_MICROHUB_FILENAME:
                    print("write file")
                    with open(REPORT_MICROHUB_FILENAME, "w") as f:
                        print("  ", REPORT_MICROHUB_FILENAME)
                        f.write("<html> <meta charset=\"UTF-8\">\n")
                        f.write(str(datetime.now()) + "<br>\n" + html_table())
                        f.write("</html>\n")
    
    # check UWB
    meta = {}
    if CHECK_UWB_STATUS:

        def run_sudo_in_dir(conn, dir, cmd):
            resp = conn.sudo(
                "bash -c 'cd " + dir + "&& " + cmd + "'",
                warn=True,
                hide="out",
            ).stdout.strip()
            return resp

        def run_command(conn, cmd):
            return conn.run(cmd, warn=True, hide="out").stdout.strip()

        for loc in combined_all:
            if "egw" not in combined_all[loc]:
                continue
            for ip in combined_all[loc]["egw"]:
                if "OFFLINE" == ip:
                    continue
                print(loc, " ", ip)
                try:
                    with Connection(
                        host=ip,
                        user="logatec",
                        connect_kwargs={"pkey": RSAKey.from_private_key_file("ssh_key")},
                        connect_timeout=20,
                    ) as conn:
                        info = {
                            "ip": ip,
                            "err": [],
                            "warn": [],
                            "num_active": run_sudo_in_dir(conn, "/home/logatec/uwb/saam/uwblib/tests", "python3 check_num_dev.py"),
                            "loc": run_command(conn, "cat /etc/lgtc/loc-id"),
                            "git": run_sudo_in_dir(conn, "/home/logatec/uwb/saam", "git rev-parse HEAD"),
                            "log": {},
                            "status": run_sudo_in_git_dir(
                                conn, "systemctl status uwb.service"
                            ),
                        }

                        # mark error
                        if info["loc"] != loc:
                            info["err"].append("LOCATION")

                        # add logs to report
                        with conn.cd("/home/logatec/uwb/saam/application"):
                            info["logs"] = run_command(conn, "ls -1 *.log")
                            for log in info["logs"].splitlines():
                                name = log.strip()
                                info["log"][name] = run_command(conn, "tail '" + name + "'")

                        # save location to meta dict
                        meta[loc] = info
                except:
                    print("connection timeout")
                    continue
            # break
            
        # send mail as html table with IPs of each location
        if SEND_UWB_MAIL or REPORT_UWB_FILENAME:
            # get smtp auth from file and send the table
            with open("auth_key.txt", "r") as auth_file:

                # we manually build html... not ideal for sure...
                def html_table():
                    # row, column, values
                    table = [
                        [
                            [""],
                            ["loc_id<br><br>IP"],
                            ["active"],
                            ["err"],
                            ["warn"],
                            ["git"],
                            ["log"],
                            ["status"],
                        ]
                    ]
                    for k in sorted(meta.keys()):
                        logs = ""
                        for l in meta[k]["log"]:
                            l_text = meta[k]["log"][l].replace("\n", "<br>")
                            if len(logs):
                                logs = logs + "<br><br>\n"
                            logs = logs + l + ": <br>" + l_text

                        table.append(
                            [
                                [k],
                                [
                                    str(meta[k]["loc"])
                                    + "<br><br>"
                                    + str(meta[k]["ip"][-4:])
                                ],
                                meta[k]["num_active"],
                                meta[k]["err"],
                                meta[k]["warn"],
                                [meta[k]["git"][:8]],
                                [logs],
                                [meta[k]["status"].replace("\n", "<br>")],
                            ]
                        )

                    txt = '<table border="01">\n'
                    for row in table:
                        txt = txt + ' <tr align="left">\n  '
                        for column in row:
                            txt = txt + "<th>" + ", ".join(column) + "</th>"
                        txt = txt + " </tr>\n"
                    return txt + "</table>"

                if SEND_UWB_MAIL:
                    _ = auth_file.readline()
                    _ = auth_file.readline()

                    sender_address = auth_file.readline()
                    sender_pass = auth_file.readline()
                    _ = auth_file.readline()
                    receiver_address = [
                        x.strip() for x in auth_file.readline().split(", ")
                    ]

                    mail_content = str(datetime.now()) + "<br>\n" + html_table()

                    message = MIMEMultipart()
                    message["From"] = sender_address
                    message["To"] = ", ".join(receiver_address)
                    message["Subject"] = "SAAM UWB logs: " + str(datetime.now())
                    message.attach(MIMEText(mail_content, "html"))
                    session = smtplib.SMTP("smtp.gmail.com", 587)  # use gmail with port
                    session.starttls()  # enable security
                    session.login(sender_address, sender_pass)
                    text = message.as_string()
                    session.sendmail(sender_address, receiver_address, text)
                    session.quit()
                    print("Mail Microhub Sent")

                if REPORT_UWB_FILENAME:
                    print("write file")
                    with open(REPORT_UWB_FILENAME, "w") as f:
                        print("  ", REPORT_UWB_FILENAME)
                        f.write("<html> <meta charset=\"UTF-8\">\n")
                        f.write(str(datetime.now()) + "<br>\n" + html_table())
                        f.write("</html>\n")
    # send mail as html table with IPs of each location
    
    if SEND_VIDEK_MAIL or REPORT_NODES_FILENAME:
        # get smtp auth from file and send the table
        with open("auth_key.txt", "r") as auth_file:

            # we manually build html... not ideal for sure...
            def html_table():
                # row, column, values
                table = [[["LOCATION"], ["EGW"], ["PMC"], ["egw loc_id err"]]]
                for k in sorted(combined_all.keys()):
                    loc = combined_all[k]
                    table.append(
                        [
                            [k],
                            loc["egw"] if ("egw" in loc) else [],
                            loc["pmc"] if ("pmc" in loc) else [],
                            [meta[k]["loc"]]
                            if ((k in meta) and (meta[k]["loc"] != k))
                            else [],
                        ]
                    )

                txt = '<table border="1">\n'
                for row in table:
                    txt = txt + ' <tr align="left">\n  '
                    for column in row:
                        txt = txt + (
                            '<th bgcolor="#FF0000">'
                            if ("OFFLINE" in column)
                            else "<th>"
                        )
                        txt = txt + ", ".join(column)
                        txt = txt + "</th>"
                    txt = txt + " </tr>\n"
                return txt + "</table>"

            if SEND_VIDEK_MAIL:
                _ = auth_file.readline()
                _ = auth_file.readline()

                sender_address = auth_file.readline()
                sender_pass = auth_file.readline()
                receiver_address = [x.strip() for x in auth_file.readline().split(", ")]

                mail_content = str(datetime.now()) + "<br>\n" + html_table()

                message = MIMEMultipart()
                message["From"] = sender_address
                message["To"] = ", ".join(receiver_address)
                message["Subject"] = "SAAM Videk nodes: " + str(datetime.now())
                message.attach(MIMEText(mail_content, "html"))
                session = smtplib.SMTP("smtp.gmail.com", 587)  # use gmail with port
                session.starttls()  # enable security
                session.login(sender_address, sender_pass)
                text = message.as_string()
                session.sendmail(sender_address, receiver_address, text)
                session.quit()
                print("Mail SAAM Sent")

            if REPORT_NODES_FILENAME:
                print("write file")
                with open(REPORT_NODES_FILENAME, "w") as f:
                    print("  ", REPORT_NODES_FILENAME)
                    f.write("<html> <meta charset=\"UTF-8\">\n")
                    f.write(str(datetime.now()) + "<br>\n" + html_table())
                    f.write("</html>\n")

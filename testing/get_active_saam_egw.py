#!/usr/bin/python3

import requests
import json


def get_active_nodes_from_videk(path_to_config_file=""):
    videk_url = ""
    auth_key = ""

    # check auth_key.txt file for address and key
    with open(path_to_config_file, "r") as f:
        videk_url = f.readline().strip()
        auth_key = f.readline().strip()
    req_headers = {"Content-Type": "application/json", "Authorization": auth_key}

    # print("Videk addr: ", videk_url)
    # print("Use \"", auth_key, "\"as authentication key")

    resp = requests.get(videk_url, headers=req_headers)
    return json.loads(resp.text)


def get_active_egw(egws):
    egws = [x for x in egws if (x["cluster"] == "saam-lgtc-172-29-0")]

    def parse_ip_from_name(name):
        ip = name[name.find("172-29-") :].replace("-", ".")
        return ip.strip()

    # map IPs to IDs
    ip_addr = {}
    for d in egws:
        if "machine_id" in d.keys():
            ip_addr[d["machine_id"].strip()] = parse_ip_from_name(d["name"].strip())
        else:
            print("PMC does not contain machine_id\n\t{}".format(str(d)))
    return ip_addr, egws


def get_active_pmc(pmcs):
    pmcs = [x for x in pmcs if (x["cluster"] == "saam-pmc-172-29-0")]

    def parse_ip_from_name(name):
        ip = name[name.find("172-29-") :].replace("-", ".")
        return ip.strip()

    # map IPs to IDs
    ip_addr = {}
    for d in pmcs:
        if "machine_id" in d.keys():
            ip_addr[d["machine_id"].strip()] = parse_ip_from_name(d["name"].strip())
        else:
            print("PMC does not contain machine_id\n\t{}".format(str(d)))
    return ip_addr, pmcs


if __name__ == "__main__":

    print("  all")
    result = get_active_nodes_from_videk("auth_key.txt")
    for e in result:
        print(e)

    ip, meta = get_active_egw(result)
    print("  egw", len(ip.items()))
    for e in ip.items():
        print(e)

    ip, meta = get_active_pmc(result)
    print("  pmc", len(ip.items()))
    for e in ip.items():
        print(e)

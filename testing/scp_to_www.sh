#!/usr/bin/env bash
#set -x

TARGET=$(head -n 1 report_server_target.txt)
echo "${TARGET}"

#files to copy
declare -a arr=("SAAM_nodes.html" "SAAM_microhub.html" "SAAM_UWB.html")

TIMESTAMP=$(date "+%Y_%m_%d-%H_%M_%S")
echo $TIMESTAMP
for fn in "${arr[@]}"
do
    echo "$fn"
    if test -f "$fn"; then
        scp "${fn}" "${TARGET}/${fn}"
        scp "${fn}" "${TARGET}/${TIMESTAMP}_${fn}"
    fi
done

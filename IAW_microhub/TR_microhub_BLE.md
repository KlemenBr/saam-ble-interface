# Unoficial technical report<br><br>Microhub BLE protocol implementation

[Microhub implementation of BLE protocol](./microhub.py) uses underlying [common BLE SAAN device abstraction](../COMMON_abst/device.py). After setting connection parameters, device connects automatically and runs a _state_machine_step_ of the implementation. 

Protocol loosely follows definitions in the SAAM documentaiton, however it is adapted for each device to cover quirks of SW, HW and FW we have detected.

Basic implementation tries to be simple to ensure enough devices can be connected concurrently and efficiently. Large chunk of time was dedicated to ensure that implementation works on wide gamut of devices, with varying processing power and capabillites. 

## Initialization

After connection is established blue characteristics and services are cached (bluepy backend). Depending on type of device (set at initalization) we check for existance of expected characteristics and initalize communication channel. 

Steps of initialization:
- cache characteristics
- set connection parameters<sup>0</sup>
- save read and write characteristic 
- subscribe to characteristic updates if neccessary <sup>1</sup>
- put device to IDLE mode (know what state we are in, clean counters)
- after a second, put device to _M3_ (streaming) mode

When initialization is complete, service only waits for received packets

    **No M3 packets received after initalization**

    If there is more than 30 seconds passed after last packet was received (or after initialization finished), and diconnect has not been issued, we call for device disconnect.<sup>2</sup>

    This is a state that can occur on wristband due to unknown reason. Currently there is no better known solution, and device will successfuly reconnect after wristband goes through power-off&power-on procedure. 


## Waiting for packets

When packet is received it is checked, parsed and forwarded to time alignment. 

When first packet is received, we also receive all information needed for time alignment (starting counter & timestamp, reported frequency, scaling factors for sample calculations...). Up until this packet time alignment object is not initialized. 

### Check
- Check that packet was received on expected characteristic 
- Check that length of the packet is expected given the device (100B for wristband, 20B otherwise)
If length is wrong, whole packet is discarded, since something went wrong with MTU handling

#### Errors
There is not a lot of possible errors, since BLE protocol should weed out any issues occured during transit 
Wristband does seem to sometimes send bad data. Seems like some bytes are switched around 
Example (see ending od 2nd and start of 3rd chunk):

data[0.0]: 03:88:2f:64:6f:01:24:e0:01:00:00:00:10:40:02:1e:00:00:00:00

data[1.0]: 03:8a:2f:64:6f:01:24:e0:01:00:00:00:10:40:02:1e:00:00:00:**03**

data[2.0]: **00**:8c:2f:64:6f:01:24:e0:01:00:00:00:10:40:02:1e:00:00:00:00

data[3.0]: 03:8e:2f:64:6f:01:24:f0:01:00:00:00:10:40:02:1f:00:00:00:00

data[4.0]: 03:90:2f:64:6f:01:24:f0:01:00:00:00:10:40:02:1e:00:00:00:00

We discard whole packet since we cannot be sure that all the ingormation is correct.


### Parse
Parse and save counter (first sample in packet)
Parse metadata(batt, accelerometer & gyro settings)
Parse array of 10b samples to real-world numbers
    - Basic packet has 20B, and consists of 2 accelerometer and 2 gyro samples
    - Bed sensor forgoes gyro, and packst 4 acceleromet samples
    - wristband sends five 20B packets in one packet (10 accelerometer, 10 gyro)
        - however wristband also sends data with 2x reported frequency that it should, so only 5 samples from each packet are useful <sup>3</sup>

### Forward the samples
Each sample calue from parsed array is forwarded to the time alignment, together with the timestamp of the packet and calculated absolute counter.<sup>4</sup>


### LONG FOOTNOTES

0. MTU of wristband is increased with bluepy workaround to allow 100B packets

1. Wristband demands write to descriptor to advertise changes, while other devices use a old serial-like protocol from the start

2. This safeguard also disconnect when clip sensor is on charger, and is sending M1 packets, that we ignore. It resolves the issue when clip sensor is taken of the charger and is kept in M1 mode. Instead of re-sending the M3 command, we go through whole initialization process to ensure proper reset was achieved.

3. This also makes counter 2x too large and time alignment must take this into account. 

4. See time alignment TR for specifics on that. From HW perspective variation between devices and additional discrepancies between reported and actual received sampling frequency are too large. We should disregard any knowledge about frequency. On longer timescale, we could estimate frequency quite precisely with counters only.

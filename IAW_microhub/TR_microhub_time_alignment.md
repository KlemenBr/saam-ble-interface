# Unoficial technical report<br><br>Microhub eGW time alignment implementation

This is a short description of simple [time alignment algorithm](./microhub_time_align.py) for MicroHub samples received via BLE. For sanity checking and review on all stages of SAAM pipeline. 

Due to issues that were noticed when device was already deployed, this document provides implementation details to cross-check on all stages. 

## TimeAlign initialization
In this implementation, time alignment is done in an object that holds last received samples and their metadata. 

At initialization:
- missing value is defined (when BLE packets are missing, this value is inserted instead at particular counter valuee)
- sampling frequency is defined (this value is currently unused in further processing)
- MAX_COUNTER_VALUE is defined (this is maximum value that counter will reach before rollover occurs) <sup>0</sup>

On each new connection of the device new timeAlign object is created. 
This is to make sure that occurances when device is reset and reconnected counters are definitely reset. If device is reconnected starting counters are unknown, and any sensible time alignment is moot.<sup>1</sup>

## New BLE sample received
Each received BLE packet is parsed before arriving to the _addSample_ method. 

Important metadata that must be provided with each new sample are:
- sample value
- sample counter (should be between 0 and MAX_COUNTER_VALUE)
- timestamp (currently not used. This is usually not actual absolute time when sample was taken, but rather time when sample arrived in a packet (together with _n_ other samples) to the device. Difference between absolute time when sample was taken and timestamps could vary up to ~30s)

At the start of sampling, we have no idea about the state of the world, so we take first sample as our ground truth. <sup>2</sup>

We save the sample value and sample counter to the queue and return from the function. 

On next received sample we calculate difference between current and previous counter<sup>4</sup>. If difference is not equal to 1, we add _missing_value_ samples to the queue, until difference in counter is 1. <sup>1, 3</sup>

## Slicing stream into 10s chunks
We are sending 10s intervals of stram to the DB. Due to the limitations described above, we do not provide chunks of equal sampling frequency. 

We use slices of stream from "t-12s" to "t-2s", to cover at least some of BLE jitter. 

We slice them based on the raw timestamp of arrived data. 
Slice is done from first sample in queue to the largest n-th sample, where 
d<sub>t</sub> = (t<sub>n</sub> - t<sub>0</sub>) and d<sub>t</sub> < 10 .

n samples are removed from queue, and sent to the DB, together with d<sub>t</sub><sup>5</sup>


## Conclusion

The "algorithm" is simplifed to ~50 lines of python code, and disregards sampling frequency and timestamps of samples. 
See footnotes on limitations of this approach, and 


### LONG FOOTNOTES

0. Due to the fact that wristband streams at two times the frequency, using the same protocol, we have different MAX_COUNTER_VALUE for devices. Time alignment protocol is not changed, since this is handled by the BLE message parser, that has more knowledge about HW & SW peculiarities -> see [TR_microhub_BLE](./TR_microhub_BLE.md).

    In short: only every second sample is sent to time alignment from wristband, and counter values are divided by two to keep time alignment algorithm unified

1. Due to the fact that BLE should diconnect after ~30 seconds of inactivity and our counter in real life do not roll around in lesser time, we currently _allow ourselves to disregard timestamps_ [see commits in ~march 2020]. **_If sampling frequency will be increased, this part must be reviewed!_**

2. This introduces quite a high error for first sample. 
First samples have usually quite high difference between actual sampling time and timestamp we detect when streaming. This is due to the fact that at the start ob BLE communication protocol is fine-tuning communication channel, and buffers are being filled. When channel is freed, a burst of packets arrive seemingly at the same time (below 7.5ms threshold as is minimum defined for BLE). This also means that there is high probability of lost packets at the start of streaming, when buffers are full and packets can be dropped on both sides. 

3. If frequency of the device would be known, we would also compare timestamps and calculate difference for each sample. Due to the fact that frequency is quite variable, we fallback to the simple time alignment using counters only <sup>1</sup>

4. There is a one-line check and fix for special case when _current counter_ < _previous counter_ . This occurs when counter rollaround happens, and is simply corrected by subtractring MAX_COUNTER_VALUE from previous counter. 

5. While timestamp and d<sub>t</sub> are both sent to the DB as fractions of a second, DB itself rounds those values to the second
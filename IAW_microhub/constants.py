
from bluepy.btle import UUID

"""characteristic UUIDs"""
microhub_UUID_name = 0x2A00

# v1 (prototype)
microhub_UUID_data = 0xFFE1

# v2 (release)
bracelet_UUID_data_r = UUID("0003cdd1-0000-1000-8000-00805f9b0131")
bracelet_UUID_data_w = UUID("0003cdd2-0000-1000-8000-00805f9b0131")
